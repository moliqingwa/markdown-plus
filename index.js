var newButton, openButton, saveButton;
var fileEntry;
var hasWriteAccess;
var previewOn = false;
var metaDictory = {"Title":"",
				"Date":"",
				"Update":"",
				"Tags":""
				};

var gui = require("nw.gui");
var fs = require("fs");

function getTodayStr() {
	var date = new Date();
	var day = date.getDate();
	var month = date.getMonth()+1;
	var year = date.getFullYear();
	return year+"-"+month+"-"+day;
}

function handlePreviewChange(onoff) {
	previewOn = onoff;
	if (previewOn) {
		document.getElementById("previewSaving").innerHTML = "已打开";
	}
	else {
		document.getElementById("previewSaving").innerHTML = "已关闭";
	}
}

function handleDocumentChange(title) {
	if (title) {
		document.getElementById("title").innerHTML = title;
	}
	else {
		document.getElementById("title").innerHTML = "[no document loaded]";
	}
}

function newFile() {
	setFile(null, false);
	handlePreviewChange(true);
	//handleDocumentChange(null);
}

function setFile(theFileEntry, isWritable) {
	fileEntry = theFileEntry;
	hasWriteAccess = isWritable;
	handleDocumentChange(fileEntry);
}

function readFileIntoEditor(theFileEntry) {
	fs.readFile(theFileEntry, function (err, data) {
		if (err) {
			console.log("Read failed: " + err);
		}
		
		var strContent = String(data);
		//handleDocumentChange(theFileEntry);
		editor.session.setValue(strContent, -1);		
	});
}

function writeEditorToFile(theFileEntry) {
	var str = editor.getValue();
	fs.writeFile(theFileEntry, str, function (err) {
    if (err) {
      console.log("Write failed: " + err);
      return;
    }

    //handleDocumentChange(theFileEntry);
    console.log("Write completed.");
  });
}

var onChosenFileToOpen = function(theFileEntry) {
  setFile(theFileEntry, true);
  readFileIntoEditor(theFileEntry);  
};

var onChosenFileToSave = function(theFileEntry) {	
	setFile(theFileEntry, true);
  
	writeEditorToFile(theFileEntry);
};

function handleNewButton() {
	setFile(null, false);
	handlePreviewChange(true);
	var str = "";
	parseMetaData(null);
	for (var key in metaDictory) {
		str += key+": "+metaDictory[key]+"\n";
	}
	str += "\n\n";	
	editor.session.setValue(str, -1);
	editor.navigateLineEnd();
	editor.focus();
}

function handleOpenButton() {
	handlePreviewChange(true);
	$("#openFile").trigger("click");
}

function handleSaveButton() {
	if (!previewOn) {
		alert("保存之前，预览必须打开!");
		return;
	}
	
	if (fileEntry && hasWriteAccess) {
		writeEditorToFile(fileEntry);
	} else {
		$("#saveFile").trigger("click");
	}
}

function handlePreviewButton() {
	var content = editor.getValue();
	if (!content)
		return;
	
	handlePreviewChange(!previewOn);
	if (previewOn) {
		console.log("editorToBlogContent");
		content = editorToBlogContent(content);
		//console.log(content);
	}
	else {
		console.log("blogContentToEditor");
		content = blogContentToEditor(content);
		//console.log(content);
	}
	
	editor.session.setValue(content, -1);
	editor.focus();
}

onload = function() {
	newButton = document.getElementsByClassName("fa-file-text-o")[0];
	openButton = document.getElementsByClassName("fa-folder-open-o")[0];
	saveButton = document.getElementsByClassName("fa-save")[0];
	previewBeforeSaveButton = document.getElementsByClassName("fa-file-text")[0];
	
	newButton.addEventListener("click", handleNewButton);
	openButton.addEventListener("click", handleOpenButton);
	saveButton.addEventListener("click", handleSaveButton);
	previewBeforeSaveButton.addEventListener("click", handlePreviewButton);
	
	$("#saveFile").change(function(evt) {
		onChosenFileToSave($(this).val());
	});
	$("#openFile").change(function(evt) {
		onChosenFileToOpen($(this).val());
	});
	
	newFile();
}

// For pelican blog content only!
parseMetaData = function(data) {
	if (data==null) {// new file
		for (var key in metaDictory) {
			if (key=="Date" || key=="Update"){
				metaDictory[key] = getTodayStr();
			}
			else {
				metaDictory[key] = "";
			}
		}
	}
	else {
		for (var key in metaDictory) {
			var regExpression = new RegExp(key+":\\s+(.*)(?!\\r\\n)");
			var match = data.match(regExpression)[1];
			if (match) {
				metaDictory[key] = match;
			}
			else {
				metaDictory[key] = "";
				console.log(key+"is not found!");
			}
			
			if (key=="Update"){
				metaDictory[key] = getTodayStr();
			}
		}
	}
}

// preview from OFF to ON
editorToBlogContent = function(data) {
	 regExpression = ""
	 retValue = data;
	var rootPath = "";
	 match = null;
	 
	 // find the root path
	 if (fileEntry) {
		 match = fileEntry.match(/(.*[\\/]content)[\\/]/);
	 }
	
	if (match && match.length < 2) {
		match = retValue.match(/\[\d+\]:\s+[\\/]?(.*)[\\/]static[\\/]/);
		if (match && match.length < 2) {
			console.log("rootPath can not be found!");
			return retValue;
		}
	}
	rootPath = match[1];
	
	// process the static image path (inverse process)
	regExpression = new RegExp("\\[(\\d+)\\]:\\s+[\\\\/]?"+rootPath.replace(/\\/g,"[\\\\/]")+"(.*)(?!\\r\\n)",'g');
	console.log(regExpression);
	retValue = retValue.replace(regExpression, 
					"[$1]: $2");
	
	regExpression = new RegExp("\\[(\\d+)\\]:(\\s+[\\\\/]?"+rootPath.replace(/\\/g,"[\\\\/]")+"(.*))(?!\\r\\n)",'g');
	match = regExpression.exec(retValue);
	while (match != null) {
		var idText = "["+match[1]+"]";
		var pathText = match[2];
		var imText = idText+":"+pathText;
		// use '/' instead of '\\'
		var repImText = imText.replace(/\\/g,"/");
		// From: "../static/..."
		// To: "/static/..."
		repImText = repImText.replace(regExpression,
						"[$1]: /"+"$3");
		console.log("imText"+imText);
		console.log("repImText"+repImText);
		
		retValue = retValue.replace(imText, repImText);
		
		match = regExpression.exec(retValue);
	}
	
	return retValue;
}

// preview from ON to OFF
blogContentToEditor = function(data) {
	parseMetaData(data);
	
	regExpression = ""
	 retValue = data;
	var rootPath = "";
	match = null;
	
	// find the root path
	if (fileEntry) {
		 match = fileEntry.match(/(.*[\\/]content)[\\/]/);
	}
	
	if (match && match.length < 2) {
		match = retValue.match(/\[\d+\]:\s+(.*)[\\/]static[\\/]/);
		if (match && match.length < 2) {
			console.log("rootPath can not be found!");
			return retValue;
		}
	}
	rootPath = match[1];
	
	// update the "Update: xxxx-xx-xx" to latest date
	retValue = retValue.replace(/Update:\s*\d{4}\s*-\s*\d+\s*-\s*\d+/,
					"Update: "+getTodayStr());
	
	// process the static image path
	regExpression = /\[(\d+)\]:(\s+)([\\/]static[\\/].*\.\w+)(?!\r\n)/g;
	match = regExpression.exec(retValue);
	while (match != null) {
		var idText = match[1];
		var pathText = match[3];
		var imText = "["+idText+"]:"+match[2]+pathText;
		// use '/' instead of '\\'
		var repImText = imText.replace(/\\/g,"/");
		pathText = rootPath.replace(/\\/g,"/");
		// From: "/static/..."
		// To: "../static/..."
		// Format 1: "[1]: /static/..."
		repImText = repImText.replace(regExpression,
						"[$1]: /"+pathText+"$3");
		console.log("imText"+imText);
		console.log("repImText"+repImText);
		retValue = retValue.replace(imText, repImText);
		
		match = regExpression.exec(retValue);
	}
	
	return retValue;
}
